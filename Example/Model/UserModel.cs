﻿namespace Model
{
    public class UserModel
    {
        public string Password { get; set; } = null!;
        public string Id { get; set; } = null!;
        public string Name { get; set; }
        public string NickName { get; set; }
        public string NumberOfGrup { get; set; }
        public string LastName { get; set; }
        public string Faculty { get; set; }
        public string Direction { get; set; }
    }
}