﻿namespace Web
{
    public static class MinimalApiRoutes
    {
        public static IServiceCollection AddMyService(this IServiceCollection services)
        {
            services.AddSingleton<IAuthService, AuthService>();
            return services;
        }
        public static WebApplication AddMinimalApiRoutes(this WebApplication app)
        {
            app.MapPost("/registration", async (UserModel user, IAuthService authService, Context context) =>
             {
                 if (context.UsersModels.FirstOrDefault((h) => h.Id == user.Id) == null)
                 {
                     await context.UsersModels.AddAsync(user);
                     await context.SaveChangesAsync();
                     return Results.Json(authService.GenerateJWtToken(user.Id));
                 }
                 return Results.StatusCode(400);
             });
            app.MapPost("/login", (Auth auth, IAuthService authService, Context context) =>
            {
                if (context.UsersModels.FirstOrDefault((h) => h.Id == auth.EMailId && h.Password == auth.Password) != null )
                {
                    return Results.Json(authService.GenerateJWtToken(auth.EMailId));
                }
                return Results.StatusCode(400);
            });
            app.MapGet("/Users",[Authorize]async (Context context) =>
             {
                 return await context.UsersModels.ToListAsync();
             });
            return app;
        }
    }
}
