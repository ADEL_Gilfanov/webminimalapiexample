﻿
namespace Web
{
    internal class AuthService : IAuthService
    {
        private IConfiguration _iConfigeration { get; }
        public AuthService(IConfiguration iConfiguration)
        {
            _iConfigeration = iConfiguration;
        }
        public TokenJWT GenerateJWtToken(string login)
        {
            var a = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(_iConfigeration["key"]!);
            var jwtDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", login) }),
                Expires = DateTime.UtcNow.AddHours(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = a.CreateJwtSecurityToken(jwtDescriptor);
            return new TokenJWT() { Token = a.WriteToken(token) };
        }
    }
}
