﻿namespace Web
{
    internal interface IAuthService
    {
        public TokenJWT GenerateJWtToken(string login);
    }
}
