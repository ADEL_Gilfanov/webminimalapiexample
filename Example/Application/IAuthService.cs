﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    internal interface IAuthService
    {
        public string GenerateJWtToken(string login);
    }
}
