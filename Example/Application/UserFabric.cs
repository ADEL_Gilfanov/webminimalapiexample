﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Essence;

namespace Application
{
    public static class UserModelFabric
    {
        public static UserModel CreateUserModel(User user)
        {
            return new UserModel { Password = user.Password, Name = user.Name, LastName = user.LastName, LoginId = user.LoginId };
        }
    }
}
