﻿global using Microsoft.EntityFrameworkCore;
global using Model;
namespace Data
{
    public class Context : DbContext
    {
        public DbSet<UserModel> UsersModels { get; set; } = null!;
        public Context(DbContextOptions<Context> options)
       : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
